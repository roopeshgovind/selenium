﻿using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TradeMe.Extensions;

namespace TradeMe
{
    public class PropertySearch
    {
        private IWebDriver _driver;        
        private const int _sleepTime = 500;
        public void Run()
        {
            _driver = new ChromeDriver(@"C:\Development\DotNet\Selenium\driver\76")
            {
                Url = "https://www.trademe.co.nz/property"
            };            

            _driver.SetSelect("PropertyRegionSelect", "1");

            System.Threading.Thread.Sleep(_sleepTime);
            _driver.SetSelect("PropertyDistrictSelect", "7");

            System.Threading.Thread.Sleep(_sleepTime);
            _driver.SetMultiSelect("PropertySuburbDiv", new List<string>() { "130", "3229" });
            //bedroom
            _driver.SetSelect("PropertyBedroomsMin", "1");
            _driver.SetSelect("PropertyBedroomsMax", "4");
            //bathroom
            _driver.SetSelect("PropertyBathroomsMin", "1");
            _driver.SetSelect("PropertyBathroomsMax", "4");

            _driver.FindElement(By.Id("forSaleButton")).Click();
        }


        #region Private Methods

        #endregion
    }
}
