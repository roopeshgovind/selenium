﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;


namespace TradeMe.Extensions
{
    public static class ChromeDriverExtension
    {
        public static void SetSelect(this IWebDriver driver, string id, string value)
        {
            var dropdownlist = new SelectElement(driver.FindElement(By.CssSelector($"select[id={id}]")));
            if(dropdownlist != null)
            {
                dropdownlist.SelectByValue(value);
            }
        }

        public static void SetMultiSelect(this IWebDriver driver, string parentId, List<string> values)
        {
            var container = driver.FindElement(By.XPath($"//div[@id='{parentId}']"));
            container.Click();
            values.ForEach(x =>
            {
                SetCheckbox(container, x);
            });

            container.Click();
        }

        #region Private Methods

        private static void SetCheckbox(IWebElement container, string value)
        {
            var checkbox = container.FindElement(By.XPath($"//input[@type='checkbox' and @value='{value}']"));
            if (checkbox != null)
            {
                var parent = checkbox.FindElement(By.XPath("./parent::*"));
                if (parent != null)
                    parent.Click();
            }
        }

        #endregion
    }
}
